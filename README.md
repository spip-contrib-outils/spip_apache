# htaccess & macros apache pour SPIP

_Divers outils et scripts pour apache et htaccess_

## macro apache `<Macro spip>` pour intégrer les règles de réécriture de SPIP dans un vhost apache à la place du .htaccess à la racine du SPIP
- fichier `000_spip.conf` à intégrer dans les fichiers de configuration d'apache 
(par ex dans /etc/apache/conf-available + lien symbolique dans /etc/apache/conf-enabled)
- exemple d'appel de la macro dans la définition d'un vhost :
```apache
<VirtualHost *:80>
	ServerName mon-site-spip.tld
	DocumentRoot "/var/www/mon-site-spip.tld/html"
	<Directory "/var/www/mon-site-spip.tld/html">
		Use spip
	</Directory>
</VirtualHost>
```

## macro apache `<Macro nG_spip>` dérivée de nG Firewall (version sans log)
- cf https://perishablepress.com/ng-firewall
- Source : 8G Firewall (https://perishablepress.com/8g-firewall/)
(cf https://git.spip.net/spip/spip/issues/5559#issuecomment-49690 pour les modifs par rapport à l'original)
- fichier `000_ng_spip.conf` à intégrer dans les fichiers de configuration d'apache 
(par ex dans /etc/apache/conf-available + lien symbolique dans /etc/apache/conf-enabled)
- exemple d'appel de la macro dans la définition d'un vhost :
(il est conseillé que l'appel de la macro nG_spip soit en début de définition du vhost)
```apache
<VirtualHost *:80>
	ServerName mon-site-spip.tld
	ServerAlias www.mon-site-spip.tld
	Use nG_spip
	...
</VirtualHost>
```

## .htaccess de SPIP avec intégration de nG_spip (sans log)
- correspond à la macro apache nG_spip
- à mettre à la racine du SPIP à la place du .htaccess existant

## macro apache `<Macro nG_spip_log>` dérivée de nG Firewall (version avec log)
- cf https://perishablepress.com/ng-firewall
- Source : 8G Firewall (https://perishablepress.com/8g-firewall/)
(cf https://git.spip.net/spip/spip/issues/5559#issuecomment-49690 pour les modifs par rapport à l'original)
- fichier `000_ng_spip_log.conf` à intégrer dans les fichiers de configuration d'apache 
(par ex dans /etc/apache/conf-available + lien symbolique dans /etc/apache/conf-enabled)
- exemple d'appel de la macro avec log : 
(il est conseillé que l'appel de la macro nG_spip_log soit en début de définition du vhost)
	- par défaut le fichier de log est **nG_log.txt** dans **...tmp/log/**
	(modifiable dans les define du début de nG_log.php)
	- il faut passer en variable le nom du fichier php de traçage (fichier à déposer à la racine du SPIP)
	exemple avec **nG_spip_log.php** comme fichier de traçage : 
```apache
<VirtualHost *:80>
	ServerName mon-site-spip.tld
	ServerAlias www.mon-site-spip.tld
	Use nG_spip_log nG_spip_log.php
	...
</VirtualHost>
```
- ref pour le fonctionnement des logs : cf https://perishablepress.com/ng-firewall-logging/




